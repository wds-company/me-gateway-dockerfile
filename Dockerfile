FROM wdsregistry1.beebuddy.net:9900/tb-me-gateway:v1.3.0

# Install restheart
ADD Files/java.tar.gz /opt/stack/
ADD Files/restheart.tar.gz /opt/stack/
ADD Files/default.properties /opt/stack/restheart/etc/default.properties
ADD Files/restheart.yml /opt/stack/restheart/etc/restheart.yml

# Install redis
ADD Files/redis-stable.tar.gz /tmp/
RUN \
  cd /tmp/redis-stable && \
  make && \
  make install && \
  cp -f src/redis-sentinel /usr/local/bin && \
  mkdir -p /etc/redis && \
  cp -f *.conf /etc/redis && \
  rm -rf /tmp/redis-stable* && \
  sed -i 's/^\(bind .*\)$/# \1/' /etc/redis/redis.conf && \
  sed -i 's/^\(daemonize .*\)$/# \1/' /etc/redis/redis.conf && \
  sed -i 's/^\(dir .*\)$/# \1\ndir \/data/' /etc/redis/redis.conf && \
  sed -i 's/^\(logfile .*\)$/# \1/' /etc/redis/redis.conf
ADD Files/redis.conf /opt/stack/redis/config/redis.conf

# Install db-factory
RUN mkdir -p /app/mappers
ADD Files/sb-msa.jar /app/sb-msa.jar

# Setup Time Timezone
RUN echo "Asia/Bangkok" > /etc/timezone && \
    rm -f /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata

# Install Supervisord and net-tools(netstat)
RUN \
  apt-get update && \
  apt-get install -y supervisor net-tools && \
  rm -rf /var/lib/apt/lists/* && \
  sed -i 's/^\(\[supervisord\]\)$/\1\nnodaemon=true/' /etc/supervisor/supervisord.conf

# Define mountable directories.
VOLUME ["/etc/supervisor/conf.d"]

# Define working directory.
WORKDIR /etc/supervisor/conf.d

COPY Files/supervisord-restheart.conf /etc/supervisor/conf.d/restheart.conf
COPY Files/supervisord-megateway.conf /etc/supervisor/conf.d/megateway.conf
COPY Files/supervisord-db-factory.conf /etc/supervisor/conf.d/supervisord-db-factory.conf
COPY Files/supervisord-filebrowser.conf /etc/supervisor/conf.d/supervisord-filebrowser.conf

# Disable redis
#COPY Files/supervisord-redis.conf /etc/supervisor/conf.d/redis.conf

ADD Files/fileconsole.tar.gz /

# Define default command.
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]
